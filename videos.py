from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys

PAGINA = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <h1>CANALES: </h1>
        <ul>
            {videos}
        </ul>
    </body>
</html>
"""


class Youtube(ContentHandler):

    def __init__(self):
        super().__init__()
        self.lista_videos = ""
        self.enEntrada = False
        self.enTitulo = False
        self.enlace = ""
        self.titulo = ""
        self.contenido = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.enEntrada = True
        elif self.enEntrada and name == 'title':
            self.enTitulo = True
        elif self.enEntrada and name == 'link':
            self.enlace = attrs.get('href')

    def endElement(self, name):
        if name == 'entry':
            self.enEntrada = False
            self.lista_videos += f" <li><a href='{self.enlace}'>{self.titulo}</a></li>\n"
            self.enlace = ""
            self.titulo = ""
        elif self.enEntrada and name == 'title':
            self.titulo = self.contenido
            self.contenido = ""
            self.enTitulo = False

    def characters(self, chars):
        if self.enTitulo:
            self.contenido += chars


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Uso: python3 videos.py <archivo.xml>")
        print()
        print(" <archivo.xml>: Archivo del canal de Youtube")
        sys.exit(1)
    try:
        analizador = make_parser()
        youtube_handler = Youtube()
        analizador.setContentHandler(youtube_handler)
        archivo_xml = open(sys.argv[1], "r")
        analizador.parse(archivo_xml)
        pagina = PAGINA.format(videos=youtube_handler.lista_videos)
        print(pagina)
    except FileNotFoundError:
        print("Error: El archivo no existe")
